# WPA-Supplier

A small executable done to help me with the editing of the wpa_supplicant config file. Not necessarily important, but it'll help me learn Rust.

# Architecture

```mermaid
---
objects
---
mindmap
  Networks
    Network
      ssid
        textfield
        click B './test.md'
      psk
        textfield
      prio
        up-button
        down-button
    Network
      ssid
      psk
      prio


```

|dialog|
- |panel|
  - |ssid|  |up|
  - |psk|   |down|
- |panel|
  - |ssid|  |up|
  - |psk|   |down|
- |panel|
  - |ssid|  |up|
  - |psk|   |down|


# TODO

- [x] read conf file
- [x] write conf file
- [ ] show file in gui
- [x] move configs up and down
- [ ] create new config file
- [-] hash/unhash psk - non-doable
- [ ] cli args a la https://doc.rust-lang.org/book/ch12-01-accepting-command-line-arguments.html
