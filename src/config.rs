
use std::{
    fs,
    str::{Lines, Split},
};
use log;

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd, Clone)]
pub struct Setting {
    pub ssid: String,
    pub psk: String,
    pub priority: i32,
}

impl Setting {
    fn to_string(&self) -> String {
         
        //let ssidStr = format!("ssid=\"{0}\"", self.ssid);
        //let pskStr = format!("psk={0}", self.psk);
        //let prioStr = format!("priority={0}", self.priority);
        let ssidStr = self.ssid.clone();
        let pskStr = self.psk.clone();
        let prio = self.priority.clone();
        let settingStr = format!(r#"network={{
    ssid={ssidStr}
    psk={pskStr}
    priority={prio}
}}"#);
 
        return settingStr;
    }
}

#[derive(Clone, Debug)]
pub struct Config {
    pub config_file_path: String,
    pub settings: Vec<Setting>,
    pub ctrl_interface: String,
    pub group: String,
    pub update_config: String
}

impl Config {
    fn read_config_file(file_path: Option<String>) -> String {
        let content: String =
            fs::read_to_string(file_path.unwrap_or(String::from("./wpa_supplicant.conf"))).expect("Error reading config file");
    
        return content;
    }
    
    pub fn write_config_file(file_path: String, content: String) -> Result<(), std::io::Error> {
    
        fs::write(file_path, content).expect("Error writing config file");
        Ok(())
    }

    pub fn save_config(&self) -> Result<(), std::io::Error> {
        let config_str = self.to_string();
        log::debug!("saving config: {}", config_str);
        Config::write_config_file(self.config_file_path.clone(), config_str)
    }
    
    fn get_ctrl_group(config: &Vec<&str>) -> String {
        let index = config.iter()
            .position(|&r| r.contains("GROUP="))
            .unwrap();

        let group_row: String = config[index].to_string();
        let groupId: usize = group_row.find('=').unwrap();
        group_row[{groupId+1}..].to_string()
    }
    
    fn get_ctrl_interface(config: &Vec<&str>) -> String {
        let index = config
            .iter()
            // .position(|&r| r == "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev")
            .position(|&r| r.contains("ctrl_interface"))
            .unwrap();
    
        let ctrl_row: String = config[index].to_string();
    
        let ctrlId = ctrl_row.find('=').unwrap();
        ctrl_row[{ ctrlId + 1 }..].to_string()
    }

    fn get_update_config(config: &Vec<&str>) -> String {
        let index = config
            .iter()
            // .position(|&r| r == "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev")
            .position(|&r| r.contains("update_config"))
            .unwrap();

        let update_row = config[index].to_string();

        let id = update_row.find('=').unwrap();
        update_row[{id + 1}..].to_string()
    }
    
    fn trim_whitespace_v1(s: &str) -> String {
        // first attempt: allocates a vector and a string
        let words: Vec<_> = s.split_whitespace().collect();
        words.join(" ")
    }
    
    fn convert_string_to_setting(configString: Vec<&str>) -> Setting {
        let mut result = Setting {
            ssid: "".to_string(),
            psk: "".to_string(),
            priority: 0,
        };
    
        for n in 1..configString.len() {
            let row = Config::trim_whitespace_v1(configString[n]);
            if row.len() >= 3 {
                // shortest I can think of is s=1
                let keyval: Vec<&str> = row.split("=").collect();
                let key = Config::trim_whitespace_v1(keyval[0]);
                let val = Config::trim_whitespace_v1(keyval[1]);
    
                let matcher = key.as_str();
    
                if key != "" && val != "" {
                    match matcher {
                        "ssid" => result.ssid = val,
                        "psk" => result.psk = val,
                        "priority" => result.priority = val.parse::<i32>().unwrap(),
                        &_ => println!("nope"),
                    }
                }
            }
        }
    
        return result;
    }
    
    fn convert_string_to_settings_list(config_string: String) -> Vec<Setting> {
        // The first in this list contains ctrl_interface and other stuff
        let settings: Vec<&str> = config_string.split("network=").collect();
    
        let mut result: Vec<Setting> = Vec::new();
    
        for cID in 1..settings.len() {
            // for configObj in configs {
            let configRows: Vec<&str> = settings[cID].split("\n").collect();
    
            // result = convert_string_to_setting(configRows);
            // println!("test");
    
            result.push(Config::convert_string_to_setting(configRows));
        }
    
        return result;
    }
    
    fn sort_settings(&mut self) {
        self.settings.sort_by(|a,b|a.priority.cmp(&b.priority));
    }
    
    pub fn promote_setting(&mut self, id: usize) {  
        if id > self.settings.len()
            {return}

        
        self.settings[id-1].priority +=1;
        self.settings[id].priority -=1;

        self.sort_settings();

    }

    pub fn new(file_path: Option<String>) -> Self {
        let config_string: String = Config::read_config_file(file_path.clone());


        let configRows: Vec<&str> = config_string.split("\n").collect();

        let mut config: Config = {Config {
            ctrl_interface: Config::get_ctrl_interface(&configRows),
            group: Config::get_ctrl_group(&configRows),
            update_config: Config::get_update_config(&configRows),
            settings: Config::convert_string_to_settings_list(config_string),
            config_file_path: file_path.unwrap_or(String::from("./wpa_supplicant.conf"))
        }};

        config.sort_settings();
        return config
    }

    pub fn to_string(&self) -> String {
        let mut result: String = "".to_string();
        let ctrl = format!("ctrl_interface={}\n", &self.ctrl_interface);
        let group = format!("GROUP={}\n", &self.group);
        let update_config = format!("update_config={}\n", &self.update_config);

        println!("ctrl: {0}", ctrl);

        println!("ctrl: {0}", ctrl);

        result.push_str(&ctrl);
        result.push_str(&group);
        result.push_str(&update_config);
        result.push_str("\n");

//        let mut counter = 0;
        for setting in self.settings.clone().into_iter() {
            let settingStr = setting.to_string();

            result.push_str(&settingStr);
            result.push_str("\n");

//            counter+=1;

        }

        return result.to_string()
   }

}



/** ---------------------------------- TESTS ---------------------------------- */

#[cfg(test)]
mod tests {
    use super::{Config, Setting};

    #[test]
    pub fn test_setting_to_string() {
        let config = Config::new(None);
        let str = config.settings[0].to_string();

        assert_eq!(r#"network={
    ssid="HOME-LAN"
    psk=asdöflkje
    priority=2
}"#, str);
    }

    #[test]
    pub fn test_config_to_string() {
        let config = Config::new(None);
        let str = config.to_string();

        let test = r#"ctrl_interface=DIR=/var/run/wpa_supplicant
GROUP=netdev
update_config=1

network={
    ssid="HOME-LAN"
    psk=asdöflkje
    priority=2
}
network={
    ssid="<PHONE-HOTSPOT>"
    psk=aölkjrioh
    priority=3
}
"#;


       assert_eq!(str, test);
    }

    #[test]
    pub fn test_promote_setting() {
        let mut config = Config::new(None);

        let ssid = config.settings[0].ssid.clone();

        config.promote_setting(1);

        assert_ne!(ssid, config.settings[0].ssid);
    }

    #[test]
    pub fn test_get_config() {
        let config = Config::new(None);

        assert!(config.settings.len() > 0);
        assert!(config.ctrl_interface.len() > 0);
    }

//    #[test]
//    pub fn test_save() {
//    let config = Config::new(None);
//      let s = config.to_string();
//
//      // println!("s: {0}", s);
//  
//        let testStr = r#"ctrl_interface=DIR=/var/run/wpa_supplicant
//GROUP=netdev
//update_config=1
//
//network={
//	ssid="<PHONE-HOTSPOT>"
//	psk=aölkjrioh
//	priority=3
//}
//
//network={
// 	ssid="HOME-LAN"
// 	psk=asdöflkje
// 	priority=2
//}
//
//        "#;
//
//        assert_eq!(s, testStr);
//    }

   #[test]
   pub fn test_save() {
    let config = Config::new(None);

    let result = config.save_config();

    assert!(result.is_ok());

   }


  #[test]
  pub fn test_convert_string_to_setting() {
    // network={    
	//   ssid="<PHONE-HOTSPOT>"
    //   psk=123
    //   priority=3
    // }
    let test_str = r#"

    ctrl_interface=DIR=/var/run/wpa_supplicant
    GROUP=netdev
    update_config=1
    
    network={
     	ssid="HOME-LAN"
        psk=asdf
        priority=2
    }"#;
  
    let configRows: Vec<&str> = test_str.split("\n").collect();
 
    let config = Config::convert_string_to_setting(configRows);
  
    assert_eq!(config.ssid, "\"HOME-LAN\"");
    assert_eq!(config.psk, "asdf");
    assert_eq!(config.priority, 2);
  }

  #[test]
  pub fn test_convert_string_to_settings_list() {
      let test_str = r#"
    ctrl_interface=DIR=/var/run/wpa_supplicant
    GROUP=netdev
    update_config=1
  
    network={
      ssid="öh"
      psk=123
      priority=3
    }

    network={
      ssid="alle"
      psk=3214342
      priority=1
    }"#;
    
    let test_string = test_str.to_string();
  
    let configs: Vec<Setting> = Config::convert_string_to_settings_list(test_string);
      assert_eq!(configs[0].ssid, "\"öh\"");
      assert_eq!(configs[0].psk, "123");
      assert_eq!(configs[0].priority, 3);
  }


  #[test]
  fn test_get_ctrl_group() {
    let config_rows = vec![
        "ctrl_interface=DIR=/var/run/wpa_supplicant",
        "GROUP=netdev"];

    let group: String = Config::get_ctrl_group(&config_rows);

//      let group: String = Config::get_ctrl_group(r#"ctrl_interface=DIR=/var/run/wpa_supplicant
//GROUP=netdev"#.to_string());

      assert_eq!(group, "netdev")
  }
  
  #[test]
  fn test_get_ctrl_interface() {

      //let input = r#"ctrl_interface=DIR=/var/run/wpa_supplicant
      //  GROUP=netdev
      //  update_config=1"#;
      //let inputRows: Vec<&str> = input.split(";").collect();
      //let ctrl_interface = Config::get_ctrl_interface(&inputRows);
      //assert_eq!(ctrl_interface, "DIR=/var/run/wpa_supplicant");

      let test_config: Vec<&str> = vec![
        "ctrl_interface=DIR=/var/run/wpa_supplicant",
        "GROUP=netdev",
        "update_config=1",
        "network={"
    ];

    let ctrl_interface: String = Config::get_ctrl_interface(&test_config);
    assert_eq!(ctrl_interface, "DIR=/var/run/wpa_supplicant");
  }

  #[test]
  pub fn test_get_update_config() {
      let config_rows = vec![
      "ctrl_interface=DIR=/var/run/wpa_supplicant",
      "GROUP=netdev",
      "update_config=1"];

      let update_config = Config::get_update_config(&config_rows);

      assert_eq!(update_config, "1")
  }
  
  #[test]
  pub fn test_read_config_file() {
      let content: String = Config::read_config_file(None);
      let f: Option<usize> = content.find("ssid=\"<PHONE-HOTSPOT>\"");
      if let Some(c) = &f {
          println!("config read: {}", c);
      }
  
      assert!(f != None)
  }
  
  #[test]
  pub fn test_write_config_file() {
      let test_str = r#"

ctrl_interface=DIR=/var/run/wpa_supplicant
GROUP=netdev
update_config=1

network={
    ssid="öh"
    psk=123
    priority=3
}"#;
  
      Config::write_config_file(
          String::from("./test_file.conf"), 
          test_str.to_string())
        .unwrap();
  }
  
  #[test]
  pub fn test_fail_read() {
      let content: String = Config::read_config_file(None);
      let f: Option<usize> = content.find("Hjalle");
  
      if let Some(c) = &f {
          println!("config read failed: {}", c);
      }
  
      assert!(f == None)
  }
  
}
