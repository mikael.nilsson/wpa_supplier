use base64::prelude::*;

pub fn encode(inval: String) -> String {
    return BASE64_STANDARD.encode(inval)
}

pub fn decode(inval: String) -> String {
    let result = BASE64_STANDARD.decode(inval).unwrap();
    return String::from_utf8(result).unwrap()
}


/** ---------------------------------- TESTS ---------------------------------- */

#[test]
pub fn test_encode() {
    let result = encode("uncoded".to_string());

    assert_eq!(result, "dW5jb2RlZA==".to_string());
}

#[test]
pub fn test_decode() {
    let result = decode("dW5jb2RlZA==".to_string());

    assert_eq!(result, "uncoded".to_string());
}

