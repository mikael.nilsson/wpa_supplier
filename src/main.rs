#![allow(non_snake_case)]
// import the prelude to get access to the `rsx!` macro and the `Element` type
// use std::collections::HashMap;
use cursive::Cursive;
use log::{self, set_logger};

mod config;
mod base64;
mod cursiveview;
mod fakedialog;

use config::Config;
use cursiveview::{CursiveView, AppData};

fn main() {

  let mut config = Config::new(None);
  
  // launch the app in the terminal
  let mut runner = cursive::default();
  runner.add_global_callback('q', cursive::Cursive::quit);
  runner.add_global_callback('l', cursive::Cursive::toggle_debug_console);
  
  let mut view = CursiveView {
//    config: &mut config,
  };
  
  // siv.set_user_data(fakedialog::AppData {counter: 0});
  // let mut fake = fakedialog::FakeView {};
  let siv = &mut *runner;
  siv.set_user_data(config);
  
  // cursiveview::config_dialog(config)
  
  let dialog = view.config_dialog(siv);
  
  runner.add_layer(dialog);
  // siv.add_layer(fake.fake_dialog());
  runner.run();
    
}


// fn config_dialog() -> Panel<LinearLayout> {
//   let ssid = EditView::new().fixed_width(20);
//   let psk = EditView::new().fixed_width(30);
//   let up_btn = Button::new("up", |s| s.quit());
//   let down_btn = Button::new("down", |s| s.quit());

//   let config_dialog = Panel::new()
//     .title("network")
//     .content(
//       LinearLayout::horizontal()
//         .child(
//           ListView::new()
//             .child("ssid", ssid)
//             .child("psk", psk)
//         )
//         .child(
//           LinearLayout::vertical()
//             .child(up_btn)
//             .child(down_btn)
//         )
//     ); 

//   return config_dialog
// }


// fn get_ctrl_interface(config: Vec<&str>) -> String {
//     let index = config
//         .iter()
//         // .position(|&r| r == "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev")
//         .position(|&r| r.contains("ctrl_interface"))
//         .unwrap();

//     let ctrl_row: String = config[index].to_string();

//     let ctrlId = ctrl_row.find('=').unwrap();
//     ctrl_row[{ ctrlId + 1 }..].to_string()
// }

// /** ---------------------------------- TESTS ---------------------------------- */

// #[test]
// fn test_get_ctrl_interface() {
//     let input = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev;update_config=1";
//     let inputRows: Vec<&str> = input.split(";").collect();
//     let ctrl_interface = get_ctrl_interface(inputRows);
//     assert_eq!(ctrl_interface, "DIR=/var/run/wpa_supplicant GROUP=netdev");
// }
