use cursive::traits::*;
use cursive::views::{Button, Dialog, EditView, LinearLayout, ListView, Panel, TextView, ViewRef};
use cursive::{Cursive, CursiveRunnable};
use log;

use crate::config::{Config, Setting};

pub struct FakeView {
}

#[derive(Clone, Debug)]
pub struct AppData {
  pub counter: u8,
}

impl FakeView {

    pub fn fake_dialog(&mut self) -> Dialog {
      let siv = cursive::default();
      cursive::logger::init();
      cursive::logger::set_internal_filter_level(log::LevelFilter::Error);
      

      LinearLayout::horizontal();

      // content.add_child(Button::new("save"|s|{

      log::info!("utanför knappen");
      // }));

      // let dialog = Dialog::new();
      
      let dialog = Dialog::new()
        .title("userdata-test")
        .button("uppsi", |s|{
          log::info!("test");
          // ! USERDATA MÅSTE SÄTTAS I MAIN!!!!
          s.with_user_data(|d: &mut AppData| {
            d.counter +=1;
            
          });

          let val = s.user_data::<AppData>().unwrap().counter;
          s.add_layer(Dialog::info(format!("val: {val}")))
          
        })
        .button("quit", Cursive::quit);

      return dialog;
    }

}
