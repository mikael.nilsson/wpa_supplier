use cursive::traits::*;
use cursive::views::{Button, Dialog, EditView, LinearLayout, ListView, Panel, TextView, ViewRef};
use cursive::{Cursive, CursiveRunnable};
use log;

use crate::config::{Config, Setting};

pub struct CursiveView {
//    pub config: &'a mut Config,
}

#[derive(Clone, Debug)]
pub struct AppData {

}

impl CursiveView {

    fn create_button(id: usize, label: &str) -> Button {
        let up_id: usize = id.clone();

        Button::new(label, move |siv: &mut Cursive| {
            let up_ssid_id = format!("{0}.ssid", id);
            let dn_ssid_id = format!("{0}.ssid", up_id - 1);
            let mut up_ssid_field: ViewRef<EditView> =
                siv.find_name::<EditView>(&up_ssid_id).unwrap();

            let mut dn_ssid_field: ViewRef<EditView> =
                siv.find_name::<EditView>(&dn_ssid_id).unwrap();

            let dn_ssid: String = dn_ssid_field.get_content().to_string();

            let up_ssid: String = up_ssid_field.get_content().to_string();

            let up_psk_id = format!("{0}.psk", id);
            let mut up_psk_field: ViewRef<EditView> = siv.find_name::<EditView>(&up_psk_id).unwrap();
            let up_psk = up_psk_field.get_content().to_string();

            let dn_psk_id = format!("{0}.psk", up_id - 1);
            let mut dn_psk_field: ViewRef<EditView> = siv.find_name::<EditView>(&dn_psk_id).unwrap();
            let dn_psk = dn_psk_field.get_content().to_string();

            up_ssid_field.set_content(dn_ssid);
            dn_ssid_field.set_content(up_ssid);
            up_psk_field.set_content(dn_psk);
            dn_psk_field.set_content(up_psk);


            // update model
            siv.with_user_data(|config: &mut Config| {
               log::debug!("config is updated");
               config.promote_setting(up_id);
                // newUpSsid = c.settings[up_id].ssid.clone();

                // c.clone()
            });

        })
    }


    fn config_panel(&mut self, id: usize, siv: &mut Cursive) -> Panel<LinearLayout> {
        let ssid_id = format!("{id}.ssid");
        let psk_id: String = format!("{id}.psk");
        let prio_id: String = format!("{id}.prio");

        let setting = siv.with_user_data(|data: &mut Config|
          data.settings[id].clone())
          .unwrap();

        let settings_length = siv.with_user_data(|data: &mut Config|
          data.settings.len())
          .unwrap();

        let ssid = EditView::new()
            .content(setting.ssid)
            .with_name(ssid_id)
            .fixed_width(20);
        let psk = EditView::new()
            .content(setting.psk)
            .with_name(psk_id)
            .fixed_width(30);
        let priority = EditView::new()
            .content(setting.priority.to_string())
            .fixed_width(10)
            .with_name(prio_id);

        let up_btn = CursiveView::create_button(id.clone(), "up");
        let down_btn = CursiveView::create_button(id + 1, "down");

        let settings_view = ListView::new()
            .child("ssid", ssid)
            .child("psk", psk)
            .child("priority", priority);

        let mut prio = LinearLayout::vertical();

        if id > 0 {
            prio.add_child(up_btn);
        }

        log::debug!("length: {}", settings_length);

        if id < settings_length - 1 {
            prio.add_child(down_btn);
        }

        let panel = Panel::new(LinearLayout::horizontal()
          .child(settings_view)
          .child(prio)
        );

        return panel;
    }

    pub fn config_dialog(&mut self, siv: &mut Cursive) -> Dialog {
        cursive::logger::init();
        cursive::logger::set_internal_filter_level(log::LevelFilter::Error);
        
        log::info!("showing config");


        let mut content = LinearLayout::vertical();
        // let id = 0;
        
        let settings = siv.with_user_data(|data: &mut Config| data.settings.clone()).unwrap();

        // for setting in config.settings.into_iter() {
        for id in 0..settings.len() {
            content.add_child(self.config_panel(id, siv));
        }


        content.add_child(Button::new("save", |s| {
            
            s.with_user_data(|config: &mut Config| {
                log::debug!("calling save_config");
                config.save_config().unwrap();

                // newUpSsid = c.settings[up_id].ssid.clone
                // c.clone()
            });
        }));

        content.add_child(Button::new("quit", |ss| {
            // s.with_user_data(|a: &mut AppData| {
            //   a.config.save_config();
            // });
            ss.quit()
        }));


        let dialog = Dialog::new().title("network").content(content);
//        let dialog = Dialog::new(); //.title("network").content(self.fake_panel());


        // let data = LinearLayout::vertical()
        //   .child(ListView::new()
        //     .child("ssid", ssid)
        //     .child("psk", psk)
        // );

        // let layout = LinearLayout::horizontal()
        //   .child(data)
        //   .child(prio);

        // let panel = Panel::new(layout);

        return dialog;
    }
}
